Module adds Barion Payment Gateway to Commerce Payment Gateway

REQUIREMENTS
------------

Commerce Barion payment requires external Barion PHP library

INSTALLATION
------------

 * Download Barion PHP library from [GitHub repository](https://github.com/barion/barion-web-php)
 * By default folder name of library is barion-web-php,
 so place this folder to Drupal root libraries folder.
 * After placing library install the Barion Payment Gateway
 module as you would normally install a
   contributed Drupal module.

MAINTAINERS
-----------

 * [Davor Horvacki (DavorHorvacki)](https://www.drupal.org/u/davorhorvacki)
 * [Bojan Milankovic (bojan.m)](https://www.drupal.org/u/bojanm)

Supporting organization:

 * [Studio Present](https://www.drupal.org/studio-present)
